'use strict';

// TODO: Add refresh functionality for that
module.exports.DEFAULT_TERMS = null;

module.exports.TRANSACTION_TYPES = {
  PAY_NOW: 'PAY_NOW',
  PAY_WITH_INSTALLMENTS: 'PAY_WITH_INSTALLMENTS',
  REFUND_PAYMENT: 'REFUND_PAYMENT'
};


module.exports.PRICES_FIELDS = [
  'commissionPercentage',
  'commissionMax',
  'commissionMin',

  'vatPercentage',
  'vatMax',
  'vatMin'
];
