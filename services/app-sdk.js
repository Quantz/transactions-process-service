'use strict';

const {Partner} = require('../models/Partner');
const {TermsAgreement} = require('../models/TermsAgreement');
const {Transaction} = require('../models/Transaction');
const {IRSReport} = require('../models/IRSReport');

// TODO: (TBD) Add data validation layer

// partner resource
module.exports.createPartner = (partner) => {
  return Partner.create(partner);
};

module.exports.createPartners = (partners) => {
  return module.exports.createPartner(partners);
};

module.exports.readPartner = (id) => {
  return Partner.findOne({_id: id});
};

module.exports.readPartnerByName = (name) => {
  return Partner.findOne({name}).populate('specialTerms');
};

module.exports.readPartners = () => {
  return Partner.find().populate('specialTerms');
};

module.exports.updatePartner = (id, partner) => {
  return Partner.updateOne({_id: id}, partner);
};

module.exports.updatePartners = (partners) => {
  return Partner.updateMany(partners);
};

module.exports.deletePartner = (id) => {
  return Partner.deleteOne({_id: id});
};

module.exports.deletePartners = () => {
  return Partner.deleteMany();
};

// terms resource
module.exports.createTermsAgreement = (termsAgreement) => {
  return TermsAgreement.create(termsAgreement);
};

module.exports.createTermsAgreements = (termsAgreements) => {
  return module.exports.createTermsAgreement(termsAgreements);
};

module.exports.readTermsAgreement = (id) => {
  return TermsAgreement.findOne({_id: id});
};

module.exports.readTermsAgreements = () => {
  return TermsAgreement.find();
};

module.exports.updateTermsAgreement = (id, termsAgreement) => {
  return TermsAgreement.updateOne({_id: id}, termsAgreement);
};

module.exports.updateTermsAgreements = (termsAgreements) => {
  return TermsAgreement.updateMany(termsAgreements);
};

module.exports.deleteTermsAgreement = (id) => {
  return TermsAgreement.deleteOne({_id: id});
};

module.exports.deleteTermsAgreements = () => {
  return TermsAgreement.deleteMany();
};

// transaction resource
module.exports.createTransaction = (transaction) => {
  return Transaction.create(transaction);
};

module.exports.createTransactions = (transactions) => {
  return module.exports.createTransaction(transactions);
};

module.exports.readTransaction = (id) => {
  return Transaction.findOne({_id: id});
};

module.exports.readTransactions = () => {
  return Transaction.find();
};

module.exports.updateTransaction = (id, transaction) => {
  return Transaction.updateOne({_id: id}, transaction);
};

module.exports.updateTransactions = (transactions) => {
  return Transaction.updateMany(transactions);
};

module.exports.deleteTransaction = (id) => {
  return Transaction.deleteOne({_id: id});
};

module.exports.deleteTransactions = () => {
  return Transaction.deleteMany();
};

// extras
module.exports.deleteIRSReports = () => {
  return IRSReport.deleteMany();
};
