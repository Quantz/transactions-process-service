'use strict';

const mongoose = require('mongoose');

const client = mongoose.connection;

client.on('error', error => console.error('MongoDB connection error:', error));

client.on('connected', () => console.log(`MongoDB connection successful to: '${client.db.databaseName}'`));

client.on('disconnected', () => console.log('MongoDB disconnected'));

module.exports.init = () => {
  mongoose.set('useCreateIndex', true);
  mongoose.set('debug', false);

  return mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true});
};
