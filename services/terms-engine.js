'use strict';

const _ = require('lodash');

const mergeRules = (oldRules, newRules) => {
  Object.keys(newRules).forEach(rule => {

    if (newRules[rule].operator && newRules[rule].operator === 'ADD') {
      oldRules[rule] = oldRules[rule] || 0;
      oldRules[rule] += newRules[rule].value;
    } else {
      oldRules[rule] = _.get(newRules[rule], 'value') === 0
        ? _.get(newRules[rule], 'value')
        : _.get(newRules[rule], 'value') || newRules[rule];
    }

  })
};

const ifAllTrue = (conditions, transaction) => {
  if (!conditions) return true;

  if (conditions.isWeekend) {
    const day = transaction.transactionDate.getDay();
    const isWeekend = (day === 6) || (day === 0);

    if (!isWeekend) {
      return false;
    }
  }

  if (conditions.above || conditions.above === 0) {
    if (transaction.amount <= conditions.above) {
      return false;
    }
  }

  if (conditions.below || conditions.below === 0) {
    if (transaction.amount >= conditions.below) {
      return false;
    }
  }

  return true;
};

module.exports.squashRules = (transaction) => {
  let rules = transaction.partner.terms[transaction.transactionType]; // consider do populate

  rules = _.orderBy(rules, 'priority');

  const squashed = {};

  rules.forEach((rule) => {
    for (let i = 0; i < rule.OR.length; ++i) {
      const condition = rule.OR[i];

      if (ifAllTrue(condition.AND, transaction)) {
        mergeRules(squashed, condition.rules);
        break;
      }

    }
  });

  return squashed;
};

module.exports.precess = (transaction) => {
  // use Decimal128 with decimal.js to emulate decimal rounding EXACTLY
  let commission = Math.abs(transaction.amount * (transaction.rules.commissionPercentage / 100));
  let vat = Math.abs(transaction.amount * (transaction.rules.vatPercentage / 100));
  const {isIRSReport} = transaction.rules;

  commission = _.min([transaction.rules.commissionMax, commission]);
  commission = _.max([transaction.rules.commissionMin, commission]);

  vat = _.min([transaction.rules.vatMax, vat]);
  vat = _.max([transaction.rules.vatMin, vat]);

  return {commission, vat, isIRSReport};
};
