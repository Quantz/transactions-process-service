'use strict';

const mongoose = require('./mongoose-setup');

module.exports.init = async () => {
  await mongoose.init();
  // other initializations will go here...
};
