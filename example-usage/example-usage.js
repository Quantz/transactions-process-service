'use strict';

const appConfig = require('../services/app-config');
const appSDK = require('../services/app-sdk');
const data = require('./mock-data');

// TODO: add error handling
module.exports = async () => {

  // clear db
  await appSDK.deleteTermsAgreements();
  await appSDK.deleteTransactions();
  await appSDK.deletePartners();
  await appSDK.deleteIRSReports();
  console.log('Clear database');

  // create terms agreements
  const NikeTerms = await appSDK.createTermsAgreement(data.termsAgreements.Nike);
  const HnMTerms = await appSDK.createTermsAgreement(data.termsAgreements["HnM"]);
  const OliveTerms = await appSDK.createTermsAgreement(data.termsAgreements.Olive);
  appConfig.DEFAULT_TERMS = await appSDK.createTermsAgreement(data.termsAgreements.default);
  console.log('Create terms agreements');

  // create partners
  await appSDK.createPartners(data.partners.small);
  console.log('Create SMALL partners');

  await appSDK.createPartner({...data.partners.Nike, specialTerms: NikeTerms});
  await appSDK.createPartner({...data.partners['HnM'], specialTerms: HnMTerms});
  await appSDK.createPartner({...data.partners.Olive, specialTerms: OliveTerms});
  console.log('Create BIG partners');

  const NikePartner = await appSDK.readPartnerByName('Nike');

  const NikeTransactions = [
    {
      transactionDate: new Date(2019, 11, 7), // Saturday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 1000,
      totalAmount: 1000,
      receiver: 'Some happy man',
      partner: NikePartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 10,
      totalAmount: 90,
      receiver: 'Some happy man',
      partner: NikePartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -20,
      totalAmount: -20,
      receiver: 'Some happy man',
      partner: NikePartner
    },
    {
      transactionDate: new Date(2019, 11, 9), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -10,
      totalAmount: -10,
      receiver: 'Some happy man',
      partner: NikePartner
    },
    {
      transactionDate: new Date(2019, 11, 9), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -1000,
      totalAmount: -1000,
      receiver: 'Some happy man',
      partner: NikePartner
    }
  ];

  await appSDK.createTransactions(NikeTransactions);
  console.log('Create transactions for Nike with a total of', await NikePartner.totalCharge);

  const HnMPartner = await appSDK.readPartnerByName('HnM');

  const HnMTransactions = [
    {
      transactionDate: new Date(2019, 11, 7), // Saturday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 10000,
      totalAmount: 10000,
      receiver: 'Some happy man',
      partner: HnMPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 10,
      totalAmount: 90,
      receiver: 'Some happy man',
      partner: HnMPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -2001,
      totalAmount: -2001,
      receiver: 'Some happy man',
      partner: HnMPartner
    },
    {
      transactionDate: new Date(2019, 11, 9), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -10,
      totalAmount: -10,
      receiver: 'Some happy man',
      partner: HnMPartner
    }
  ];

  await appSDK.createTransactions(HnMTransactions);
  console.log('Create transactions for HnM with a total of', await HnMPartner.totalCharge);


  const OlivePartner = await appSDK.readPartnerByName('Olive');

  const OliveTransactions = [
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 10000,
      totalAmount: 10000,
      receiver: 'Some happy man',
      partner: OlivePartner
    },
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 1101,
      totalAmount: 1101,
      receiver: 'Some happy man',
      partner: OlivePartner
    },
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 900,
      totalAmount: 900,
      receiver: 'Some happy man',
      partner: OlivePartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 1001,
      totalAmount: 9009,
      receiver: 'Some happy man',
      partner: OlivePartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -2001,
      totalAmount: -2001,
      receiver: 'Some happy man',
      partner: OlivePartner
    },
    {
      transactionDate: new Date(2019, 11, 9), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -10,
      totalAmount: -10,
      receiver: 'Some happy man',
      partner: OlivePartner
    }
  ];

  await appSDK.createTransactions(OliveTransactions);
  console.log('Create transactions for Olive with a total of', await OlivePartner.totalCharge);

  const DavidBigudPartner = await appSDK.readPartnerByName('David bigud');

  const DavidBigudTransactions = [
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 10000,
      totalAmount: 10000,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 1101,
      totalAmount: 1101,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 6),
      transactionType: appConfig.TRANSACTION_TYPES.PAY_NOW,
      amount: 900,
      totalAmount: 900,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 500,
      totalAmount: 1000,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 501,
      totalAmount: 1002,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.PAY_WITH_INSTALLMENTS,
      amount: 1001,
      totalAmount: 9009,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 8), // Sunday
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -2001,
      totalAmount: -2001,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    },
    {
      transactionDate: new Date(2019, 11, 9),
      transactionType: appConfig.TRANSACTION_TYPES.REFUND_PAYMENT,
      amount: -10,
      totalAmount: -10,
      receiver: 'Some happy man',
      partner: DavidBigudPartner
    }
  ];

  await appSDK.createTransactions(DavidBigudTransactions);

  console.log('Create transactions for David Bigud with a total of', await DavidBigudPartner.totalCharge);
};
