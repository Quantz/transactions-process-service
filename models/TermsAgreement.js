'use strict';

const mongoose = require('mongoose');

const {TRANSACTION_TYPES, PRICES_FIELDS} = require('../services/app-config');

const Schema = mongoose.Schema;

let TermsAgreement;

const PriceSchema = new Schema({
  value: {type: Number},
  operator: {type: String, enum: ['OVERWRITE', 'ADD']}
}, {_id: false});

const PRICES_FIELDS_MAP = PRICES_FIELDS.reduce((FIELDS, FIELD) => ({...FIELDS, [FIELD]: {type: PriceSchema}}), {});

const PricesSchema = new Schema({
  ...PRICES_FIELDS_MAP,
  isIRSReport: Boolean
}, {_id: false});

const ConstraintsSchema = new Schema({rules: {type: PricesSchema, default: {}}, AND: {type: Object, default: {isWeekend: true, above: 100}}}, {_id: false});

const RulesSchema = new Schema({
  priority: {type: Number, required: true, default: 1000},
  OR: {type: [ConstraintsSchema], default: {}} // isWeekend, above/below etc.
}, {_id: false});

const TRANSACTION_TYPES_MAP = Object.values(TRANSACTION_TYPES).reduce((TYPES, TYPE) => ({...TYPES, [TYPE]: {type: [RulesSchema], default: {}}}), {});

const TermsAgreementSchema = new Schema({
  description: String,
  ...TRANSACTION_TYPES_MAP
}, {timestamps: true});

TermsAgreement = mongoose.model('TermsAgreement', TermsAgreementSchema);

module.exports = {TermsAgreement};
