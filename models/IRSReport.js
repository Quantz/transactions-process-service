'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let IRSReport;

const IRSReportSchema = new Schema({
  text: {type: String, default: 'This guy is laundering money'},
}, {timestamps: true});

IRSReport = mongoose.model('IRSReport', IRSReportSchema);

module.exports = {IRSReport};
