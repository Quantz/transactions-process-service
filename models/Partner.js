'use strict';

const mongoose = require('mongoose');
const _ = require('lodash');

const {Transaction} = require('./Transaction');
const appConfig = require('../services/app-config');

const Schema = mongoose.Schema;

let Partner;

const PartnerSchema = new Schema({
  name: {type: String, required: true, unique: true},
  specialTerms: {type: Schema.Types.ObjectId, ref: 'TermsAgreement'},
}, {timestamps: true});

PartnerSchema.virtual('terms').get(function () {
  const defaultTerms = appConfig.DEFAULT_TERMS && appConfig.DEFAULT_TERMS.toJSON() || {};
  const specialTerms = this.specialTerms && this.specialTerms.toJSON() || {};

  return Object
    .values(appConfig.TRANSACTION_TYPES)
    .reduce((TYPES, TYPE) => ({...TYPES, [TYPE]: [...defaultTerms[TYPE] || [], ...specialTerms[TYPE] || []]}), {});
});

PartnerSchema.virtual('totalCharge').get(function () {
  return Transaction.aggregate([
    {$match: {partner: this._id}},
    {$group: {_id: null, amount: {$sum: "$amount"}}}
  ]).then(results => _.get(results, '[0].amount'));
});

Partner = mongoose.model('Partner', PartnerSchema);

module.exports = {Partner};
