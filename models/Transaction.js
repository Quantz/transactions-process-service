'use strict';

const mongoose = require('mongoose');

const {IRSReport} = require('./IRSReport');
const termsEngine = require('../services/terms-engine');
const {TRANSACTION_TYPES} = require('../services/app-config');

const Schema = mongoose.Schema;

let Transaction;

const TransactionSchema = new Schema({
  transactionDate: {type: Date, required: true},
  transactionType: {type: String, required: true, enum: Object.values(TRANSACTION_TYPES)},
  amount: {type: Number, required: true},
  totalAmount: {type: Number, required: true},
  receiver: {type: String, required: true},
  commission: Number, // use Types.Decimal128 to emulate decimal rounding EXACTLY
  vat: Number, // use Types.Decimal128 to emulate decimal rounding EXACTLY
  IRSReport: {type: Schema.Types.ObjectId, ref: 'IRSReport'},
  partner: {type: Schema.Types.ObjectId, ref: 'Partner', index: true},
  rules: Schema.Types.Mixed
}, {timestamps: true});

TransactionSchema.pre('save', async function () {
  this.rules = termsEngine.squashRules(this);

  const {commission, vat, isIRSReport} = termsEngine.precess(this);

  this.commission = commission;
  this.vat = vat;

  if (isIRSReport) {
    this.IRSReport = await IRSReport.create({});
  }
});

Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = {Transaction};
