# transactions-process-service

* Obviously, it would be more appropriate to use a relational database but since I'm not implementing any atomic transactions I went with the easier setup which is MongoDB with mongoose

* I am assuming weekend is Saturday-Sunday

* It took me about 4 hours and as expected the main part of the time I spent on the processing engine,
I considered the best way to implement the terms-engine and decided to go with the more generic way even though it is more complex

* The SDK contains methods that are not necessarily needed for this project but I like to keep things consistent
  And in any case, those methods are now just a nice facade but in a real-life project, probably, will not be so simple and they would be inside a controller with a little bit more logic and returns an error/success response, etc.
  
* If you want to change the terms rules for a specific partner or the small partners all you have to do is to update the corresponding document in the term DB

* If you want to add a big partner with special terms you just need to add new terms document

* You can push as many rules as you want, either to the default terms or the special ones.
  You can tweak the values in a rule, change its priority or add completely new logic without the need to migrate the DB
  
* For the sake of simplicity, I didn't I put too much thought into performance and error handling

* You can run with cloud DB or local, you can change the settings in the `environments/.env`

* The tests and examples are in the `example-usage` folder you can add/comment/uncomment parts there

* Run the app ```npm i && node .```

* I'm running on windows, hopefully, there will be no change in the behavior if you running on something else, if there is please let me know

* The database gets cleared before each time you run the app so don't worry about data conflicts and such

