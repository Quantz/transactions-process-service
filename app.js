'use strict';

const path = require('path');
'use strict';

const consoleStamp = require('console-stamp');
const dotenvFlow = require('dotenv-flow');

const startup = require('./services/app-startup');
const testApp = require('./example-usage/example-usage.js');

consoleStamp(console, {pattern: 'dd/mm/yyyy HH:MM:ss.l', colors: {stamp: 'yellow'}});

dotenvFlow.config({path: path.resolve('./environments')});

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here...
});

startup.init()
  .then(() => console.log('App initialized successfully'))
  .then(() => testApp())
  .catch(error => console.error('App crashed due to:', error));
